job "mqtt-management" {
  type = "service"
  region = "global"
  datacenters = ["proxima"]

  group "emqx" {
    count = 1

    volume "emqx-etc" {
      type = "host"
      read_only = false
      source = "emqx-etc"
    }

    volume "emqx-data" {
      type = "host"
      read_only = false
      source = "emqx-data"
    }

    volume "emqx-log" {
      type = "host"
      read_only = false
      source = "emqx-log"
    }

    constraint {
      distinct_hosts = true
    }

    network {
      port "http" { to = 8083 }
      port "mqtt" { to = 1883 }
      port "dashboard" { to = 18083 }
    }

    task "emqx" {
      driver = "docker"

      volume_mount {
        volume = "emqx-etc"
        destination = "/opt/emqx/etc"
        read_only = false
      }

      volume_mount {
        volume = "emqx-data"
        destination = "/opt/emqx/data"
        read_only = false
      }

      volume_mount {
        volume = "emqx-log"
        destination = "/opt/emqx/log"
        read_only = false
      }

      config {
        image = "emqx/emqx:4.2.11"

        ports = ["http", "mqtt", "dashboard"]
      }

      template {
        data = <<EOH
          TZ='Europe/Stockholm'
          EMQX_NAME='{{ env "node.unique.name" }}'
          EMQX_HOST='emqx.hq.carboncollins.se'
          EMQX_LOADED_PLUGINS="emqx_retainer,emqx_management,emqx_dashboard"
        EOH

        destination = "secrets/emqx.env"
        env = true
      }

      service {
        name = "emqx"
        port = "dashboard"

        tags = [
            "traefik.enable=true",
            "traefik.http.routers.emqx.entrypoints=https",
            "traefik.http.routers.emqx.rule=Host(`emqx.hq.carboncollins.se`)",
            "traefik.http.routers.emqx.tls=true",
            "traefik.http.routers.emqx.tls.certresolver=lets-encrypt",
            "traefik.http.routers.emqx.tls.domains[0].main=*.hq.carboncollins.se",
            "traefik.tcp.routers.emqx.entrypoints=mqtt",
            "traefik.tcp.routers.emqx.rule=HostSNI(`emqx.hq.carboncollins.se`)",
            "traefik.tcp.routers.emqx.tls=true",
            "hq.service.exposed=true",
            "hq.service.subdomain=emqx"
        ]

        check {
          name = "EMQX Dashboard"
          port = "dashboard"
          type = "tcp"
          interval = "30s"
          timeout = "5s"
          task = "emqx"
        }

        check {
          name = "EMQX WebSocket"
          port = "http"
          type = "tcp"
          interval = "30s"
          timeout = "5s"
          task = "emqx"
        }

        check {
          name = "EMQX MQTT TCP"
          port = "mqtt"
          type = "tcp"
          interval = "30s"
          timeout = "5s"
          task = "emqx"
        }
      }
    }
  }
}